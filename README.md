# wmcs-k8s-metrics

This is the helm chart for the wmcs-metrics component. It's part of the
Kubernetes metrics components that are now moved to the toolforge-deploy
repository.

More information:
https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Kubernetes#Monitoring
